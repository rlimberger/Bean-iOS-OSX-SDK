

#include "eventLib.h"

// Process the event loop
void processEvent()
{
    //Increment a global tick counter
    globalEvent.processTick();

    // You need to comment this out in the Bean Library for this to work
    ///Applications/Arduino-1.0.5.app/Contents/Resources/Java/hardware/LightBlue-Bean/cores/bean/Bean.cpp" line 42 
    
    const bool currentlyConnected = Bean.getConnectionState();
    
    // Processing to handle panic mode.  If we've lost connection do something special.
    if (globalEvent.checkForPanicMode)
    {
        if (currentlyConnected)
        {
            if (! globalEvent.wasConnected)
            {
                // just got a connection.  turn off panic mode.
                currentPattern = &bootSequence;
                currentPattern->initialize( globalEvent );
                mp3_play(SND_END_PANIC);
            }
        }
        else
        {
            if (globalEvent.wasConnected)
            {
                // whoops, we were connected and lost the connection... enter panic mode
                // start panic mode sound playback
                currentPattern = &hatAlarm;
                currentPattern->initialize( globalEvent );
                mp3_play(SND_BEGIN_PANIC);
            }
        }
    }
#if 0
    // Engle note... this code was causing panic mode to mess up if there are minor glitches in connection.
    // Caused by global state of colors and pulse_rate being reset here.  Not getting restored when
    // connection resumed.  TODO:  only do this if not currently panicing.  Even better solution is to make boot
    // sequence pattern not change the globals.
    else if (! currentlyConnected)    // not looking for panic but got disconnected... go to boot sequence
    {
        if (globalEvent.wasConnected) {
            bootSequence.initialize( globalEvent );
        }
        bootSequence.processAnimation( &globalEvent );
    }
#endif
        
    if (currentPattern != NULL)
    {
      currentPattern->processAnimation( &globalEvent );
    }
    
    globalEvent.wasConnected = currentlyConnected;
}


//
// ProcessEvent Class
//
    
ProcessEvent::ProcessEvent() {
    processDelay=500;
    
    //Color for LEDs
    
    //Start Color
    colorStartLeft[0]=255;
    colorStartLeft[1]=0;
    colorStartLeft[2]=0;

    colorStartRight[0]=255;
    colorStartRight[1]=0;
    colorStartRight[2]=0;
    
    //End Color 
    colorEndLeft[0]=0;
    colorEndLeft[1]=0;
    colorEndLeft[2]=0;

    colorEndRight[0]=0;
    colorEndRight[1]=0;
    colorEndRight[2]=0;
    
    // Pulse Rate for animation
    pulse_rate=COLOR_INCREMENT_RATE;
    pulse_dir=pulse_rate;   
   
    //Global counter
    tickCount=0;
    
    checkForPanicMode=false;
    wasConnected = false;
}


void ProcessEvent::processTick() 
{
    tickCount++;
    
    if ( tickCount >= TICKS_PER_SECOND )
        tickCount=0;
}
void ProcessEvent::setColor( boolean ear, uint8_t red, uint8_t green, uint8_t blue )
{
    switch ( ear )
    {
        default:
        case EAR_LEFT:
        {
            colorStartLeft[0]=red;
            colorStartLeft[1]=green;
            colorStartLeft[2]=blue;
            colorEndLeft[0]=red;
            colorEndLeft[1]=green;
            colorEndLeft[2]=blue;
        }
        break;
        case EAR_RIGHT:
        {
            colorStartRight[0]=red;
            colorStartRight[1]=green;
            colorStartRight[2]=blue;
            colorEndRight[0]=red;
            colorEndRight[1]=green;
            colorEndRight[2]=blue;
        }
        break;
    }
}

void ProcessEvent::setBeginColor( boolean ear, uint8_t red, uint8_t green, uint8_t blue )
{
    switch ( ear )
    {
        default:
        case EAR_LEFT:
        {
            colorStartLeft[0]=red;
            colorStartLeft[1]=green;
            colorStartLeft[2]=blue;
        } 
        break;
        case EAR_RIGHT:
        {
            colorStartRight[0]=red;
            colorStartRight[1]=green;
            colorStartRight[2]=blue;
        }
        break;
    }
}

void ProcessEvent::setEndColor( boolean ear, uint8_t red, uint8_t green, uint8_t blue )
{
    switch ( ear )
    {
        default:
        case EAR_LEFT:
        {
            colorEndLeft[0]=red;
            colorEndLeft[1]=green;
            colorEndLeft[2]=blue;
        } 
        break;
        case EAR_RIGHT:
        {
            colorEndRight[0]=red;
            colorEndRight[1]=green;
            colorEndRight[2]=blue;
        }
        break;
    }
}
