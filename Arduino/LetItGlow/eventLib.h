

#ifndef EVENT_LIB_H
#define EVENT_LIB_H

#include "Arduino.h"
#include "global.h"

class ProcessEvent
{
 public:

    void processTick();
    
    ProcessEvent();
    
    void setColor( boolean ear, uint8_t red, uint8_t green, uint8_t blue );
    void setBeginColor( boolean ear, uint8_t red, uint8_t green, uint8_t blue );
    void setEndColor( boolean ear, uint8_t red, uint8_t green, uint8_t blue );

//    float transitionDuration;
    int processDelay;
    
    uint8_t colorStartLeft[3], colorStartRight[3]; 
    uint8_t colorEndLeft[3], colorEndRight[3]; 

    int pulseCount;
    uint8_t pulse_rate;
    int8_t pulse_dir;
    
    int tickCount;
    
    bool checkForPanicMode;
    bool wasConnected;
};

#endif
