

#ifndef PATTERN_LIB_H
#define PATTERN_LIB_H

#include "Arduino.h"
#include <SoftPWM.h>

// Project libraries
#include "eventLib.h"
#include "global.h"

Timer t;
int timerId;
bool debug=false;


void clearAllColors( ProcessEvent& globalEvent );

void setLed( int ear, uint8_t red, uint8_t green, uint8_t blue );

class Pattern
{
 public:
    float transitionDuration;
    int processDelay;
    int localCounter;
    Pattern() {
        processDelay=250;
        localCounter = 0;
    }
    virtual void initialize( ProcessEvent& globalData ) {}
    virtual void processAnimation( ProcessEvent *globalData ) = 0;
};

class PingPong : public Pattern
{
 public:
    PingPong() : Pattern() {
        //processDelay=500;
    }

    void processAnimation( ProcessEvent *globalData )
    {
         //Ping pong three times in red
        if ( localCounter == 0 ) {
            setLed( EAR_LEFT, globalData->colorStartLeft[0], globalData->colorStartLeft[1], globalData->colorStartLeft[2] );
            setLed( EAR_RIGHT, 0, 0, 0 );
            //delay(processDelay);
        } else if ( localCounter == globalData->pulse_rate ) {
            setLed( EAR_LEFT, 0, 0, 0 );
            setLed( EAR_RIGHT, globalData->colorStartRight[0], globalData->colorStartRight[1], globalData->colorStartRight[2] );
            //delay(processDelay);
        }
                
        localCounter++;
        if ( localCounter >= globalData->pulse_rate*2 )
            localCounter = 0;
    }
};


class CycleColors : public Pattern
{
 public:
    CycleColors() : Pattern() {
        //processDelay=500;
    }

    void processAnimation( ProcessEvent *globalData )
    {
        if ( localCounter == 0 )
        {
            // Cycle color for both ears
            setLed( EAR_LEFT, 255, 0, 0 );
            setLed( EAR_RIGHT, 255, 0, 0 );
            //delay(processDelay);
        } else if ( localCounter == globalData->pulse_rate ) {
            setLed( EAR_LEFT, 0, 255, 0 );
            setLed( EAR_RIGHT, 0, 255, 0 );
            //delay(processDelay);
        } else if ( localCounter == globalData->pulse_rate*2 ) {
            setLed( EAR_LEFT, 0, 0, 255 );
            setLed( EAR_RIGHT, 0, 0, 255 );
            //delay(processDelay);  
        }
        
        localCounter++;
        if ( localCounter >= globalData->pulse_rate*3 )
            localCounter = 0;
    }
};

class BlinkColors : public Pattern
{
 public:
    BlinkColors() : Pattern() {
        processDelay=50;
    }

    void processAnimation( ProcessEvent *globalData )
    { 
//        Serial.print("Blink ");
//        Serial.print(globalData->tickCount);
//        Serial.print(" ");       
//        Serial.print(processDelay);
//        Serial.print(" "); 
//        Serial.println(globalData->tickCount%(processDelay*2));
        
        if ( localCounter == 0 ) {
            setLed( EAR_LEFT, globalData->colorStartLeft[0], globalData->colorStartLeft[1], globalData->colorStartLeft[2] );
            setLed( EAR_RIGHT, globalData->colorStartRight[0], globalData->colorStartRight[1], globalData->colorStartRight[2] );
            //delay(processDelay);
        } else if ( localCounter == globalData->pulse_rate ) {
            setLed( EAR_LEFT, 0, 0, 0 );
            setLed( EAR_RIGHT, 0, 0, 0 );
            //delay(processDelay);
        }
        localCounter++;
        if ( localCounter >= globalData->pulse_rate*2 )
            localCounter = 0;
    }
};

class SetColor : public Pattern
{
 public:
    SetColor() : Pattern() {
        //processDelay=500;
    }

    void initialize()
    {
    }
    
    void processAnimation( ProcessEvent *globalData )
    {     
        if ( localCounter == 0 ) {
            //change ear colors to current 'start' color.
            setLed( EAR_LEFT, globalData->colorStartLeft[0], globalData->colorStartLeft[1], globalData->colorStartLeft[2] );
            setLed( EAR_RIGHT, globalData->colorStartRight[0], globalData->colorStartRight[1], globalData->colorStartRight[2] );
        } 
        
        if ( globalData->pulse_rate > 0 && localCounter >= globalData->pulse_rate ) {
            // change ear colors to black.
            clearAllColors( *globalData );
            setLed( EAR_LEFT, globalData->colorStartLeft[0], globalData->colorStartLeft[1], globalData->colorStartLeft[2] );
            setLed( EAR_RIGHT, globalData->colorStartRight[0], globalData->colorStartRight[1], globalData->colorStartRight[2] );
        }
        localCounter++;
//        if ( localCounter >= globalData->pulse_rate*2 )
//            localCounter = 0;
    }
};


class PulseColor : public Pattern
{
 public:
    PulseColor() : Pattern() {
        //processDelay=500;
    }
    void processAnimation( ProcessEvent *globalData );
};

class MorsePattern : public Pattern
{
  public:
    MorsePattern( const uint8_t onoff[], int numdots ) : Pattern() {
      m_onoff = onoff;
      m_numdots = numdots;
    }    
    
    void initialize( ProcessEvent& globalEvent )
    {
      localCounter = 0;
    }
    
    void processAnimation( ProcessEvent *globalData )
    {
      if (localCounter % globalData->pulse_rate == 0)
      {
        unsigned int pos = localCounter / globalData->pulse_rate;
        if (pos >= m_numdots)
        {
          localCounter = 0;
          pos = 0;
        }
        if (m_onoff[pos])
        {
          // "on" color
          setLed( EAR_LEFT, globalData->colorStartLeft[0], globalData->colorStartLeft[1], globalData->colorStartLeft[2] );
          setLed( EAR_RIGHT, globalData->colorStartRight[0], globalData->colorStartRight[1], globalData->colorStartRight[2] );
        }
        else
        {
          // "off" color
          setLed( EAR_LEFT, globalData->colorEndLeft[0], globalData->colorEndLeft[1], globalData->colorEndLeft[2] );
          setLed( EAR_RIGHT, globalData->colorEndRight[0], globalData->colorEndRight[1], globalData->colorEndRight[2] );            
        }
      }
      ++localCounter;
    }
    
  private:
    const uint8_t *m_onoff;
    int m_numdots;
};

// Morse code for "SOS"
static const uint8_t sos_pattern[] = {
  1,0,1,0,1,              // 'S'
  0,0,0,                  // char space
  1,1,1,0,1,1,1,0,1,1,1,  // 'O'
  0,0,0,                  // char space
  1,0,1,0,1,              // 'S'
  0,0,0,0,0,0,0           // word space
};
class HatAlarm : public MorsePattern
{
 public:
    HatAlarm() : MorsePattern(sos_pattern, sizeof(sos_pattern)) {
    }
    
    void initialize( ProcessEvent& globalEvent )
    {
        // "on" color = yellow
        globalEvent.setBeginColor( EAR_LEFT, 255, 255, 0 );
        globalEvent.setBeginColor( EAR_RIGHT, 255, 255, 0 );
        
        // "off" color = black
        globalEvent.setEndColor( EAR_LEFT, 0, 0, 0 );
        globalEvent.setEndColor( EAR_RIGHT, 0, 0, 0 );
        
        globalEvent.pulse_rate = 10;
        MorsePattern::initialize(globalEvent);
    }
};

class TurnEarsOff : public Pattern
{
 public:
    TurnEarsOff() : Pattern() {
        //processDelay=500;
    }
    
    void initialize( ProcessEvent& globalData ) 
    {
     
    }
    void processAnimation( ProcessEvent *globalData )
    {     
        //Set both leds to OFF
        setLed( EAR_LEFT, 0, 0, 0 );
        setLed( EAR_RIGHT, 0, 0, 0 );
    }
};

class BootSequence : public Pattern
{
 public:
    BootSequence() : Pattern() {
        //processDelay=500;
    }
    void initialize( ProcessEvent& globalEvent )
    {
        globalEvent.setBeginColor( EAR_LEFT, 0, 255, 255 );  // cyan
        globalEvent.setBeginColor( EAR_RIGHT, 0, 255, 255 );
        globalEvent.setEndColor( EAR_LEFT, 0, 0, 0 );
        globalEvent.setEndColor( EAR_RIGHT, 0, 0, 0 );
        globalEvent.pulse_rate = 35;
    }
    void processAnimation( ProcessEvent *globalData );
};
#endif

