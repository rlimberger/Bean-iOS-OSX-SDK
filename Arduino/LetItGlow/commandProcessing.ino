
#include "commandProcessing.h"
#include "initialize.h"

const char compile_date[] = __DATE__ " " __TIME__;
void printHelp()
{
    Serial.print("Compile time: ");
    Serial.println(compile_date);
    Serial.println("Commands:");
    Serial.println("   A-alarm");
    Serial.println("   B-blinkColors");
    Serial.println("   C[L,R,B][S,E,B]R,G,B[,duration]");
    Serial.println("   D-debug");
    Serial.println("   I0|1-Simulated Panic Mode (off/on)");
    Serial.println("   J[0123RGBY][LRB]-Simon Color");
    Serial.println("   O0|1-checkForPanicMode");
    Serial.println("   P-pulseColor");
    Serial.println("   p-pingPingColor");
    Serial.println("   Q[L,R,B][S,E,B]R,G,B[,duration]");
    Serial.println("   R-pulse_rate");
    Serial.println("   S#=play song #");
    Serial.println("   T#-Change the timer");
    Serial.println("   V#-Volume");
    Serial.println("   Z-Turn all LEDs off");
    Serial.println("   z-Display boot sequence");
    Serial.println("   H-Help");
    Serial.println("   1-LetItGlow Pattern");
}


void printGlobalEvent( ProcessEvent& globalEvent )
{     
    Serial.println("Processing Data:");

    Serial.print("  processDelay");
    Serial.println(globalEvent.processDelay);
    
    //Start Color
    Serial.print("  colorStartLeft");
    Serial.print(globalEvent.colorStartLeft[0]);
    Serial.print(" ");
    Serial.print(globalEvent.colorStartLeft[1]);
    Serial.print(" ");
    Serial.println(globalEvent.colorStartLeft[2]);

    Serial.print("  colorStartRight=");
    Serial.print(globalEvent.colorStartRight[0]);
    Serial.print(" ");
    Serial.print(globalEvent.colorStartRight[1]);
    Serial.print(" ");
    Serial.println(globalEvent.colorStartRight[2]);
    
    //End Color 
    Serial.print("  colorEndLeft=");
    Serial.print(globalEvent.colorEndLeft[0]);
    Serial.print(" ");
    Serial.print(globalEvent.colorEndLeft[1]);
    Serial.print(" ");
    Serial.println(globalEvent.colorEndLeft[2]);

    Serial.print("  colorEndRight=");
    Serial.print(globalEvent.colorEndRight[0]);
    Serial.print(" ");
    Serial.print(globalEvent.colorEndRight[1]);
    Serial.print(" ");
    Serial.println(globalEvent.colorEndRight[2]);
    
    // Pulse Rate for animation
    Serial.print("  pulse_rate=");
    Serial.println(globalEvent.pulse_rate);
    
    Serial.print("  pulse_dir=");
    Serial.println(globalEvent.pulse_dir);   
   
    //Global counter
    Serial.print("  tickCount");
    Serial.print(globalEvent.tickCount);
}

char buffer[64];
int bufferCounter=0;
    
void processInput()
{   
    // Process the input bytes until we fill up the string

    if ( Serial.available() ) {
        const char readChar = Serial.read();
        if (readChar == '\r')
            ; // do nothing
        else if (readChar != '\n')
            buffer[bufferCounter++] = readChar;   // add it to input buffer
        else if (bufferCounter >= 1)
        {
            int i_value=-1;      
    
            // Make a String object from a char*
            buffer[bufferCounter] = '\0'; 
            const String argsStr( buffer+1 );
            //Serial.println(argsStr);
     
            char command = buffer[0];   
            switch( command )
            {
                case '1':
                {
                    globalEvent.setBeginColor( EAR_LEFT, 128, 128, 255 );
                    globalEvent.setEndColor( EAR_LEFT, 0, 0, 0 );
                    globalEvent.setBeginColor( EAR_RIGHT, 128, 128, 255 );
                    globalEvent.setEndColor( EAR_RIGHT, 0, 0, 0 );
                    currentPattern=&pulseColor;
                    globalEvent.pulse_rate = 5;
                    mp3_play(SND_LETITIGO);
                }
                break;
                case 'A':
                    //i_value = argsStr.toInt();
                    currentPattern=&hatAlarm;
                    currentPattern->initialize( globalEvent );
                    //Alarm
                    //Time: word R: byte G: byte B: byte V: bool Duration
                    //A(480, 255, 34, 18, 1, 10)
                break;
                case 'B':
                    currentPattern=&blinkColors;
                break;
                case 'C':
                    parseColor( argsStr );
                break;
                case 'd':            
                case 'D':
                     debug = !debug;
                     Serial.print("debug=");
                     Serial.println(debug);
                break;
                case 'I':   // enter/exit simulated panic mode
                    i_value = argsStr.toInt();
                    if (i_value == 0)
                    {
                        currentPattern=&bootSequence;
                        currentPattern->initialize( globalEvent );
                        mp3_play(SND_END_PANIC);
                    }
                    else
                    {
                        currentPattern=&hatAlarm;
                        currentPattern->initialize( globalEvent );
                        mp3_play(SND_BEGIN_PANIC);
                    }
                break;
                case 'J':
                    {
                      int snd, r, g, b;
                      switch (argsStr[0]) {
                        case '0':  // red
                        case 'R':
                          r = 255; g = 0; b = 0;
                          snd = 3;
                          break;
                        case '1':  // green
                        case 'G':
                          r = 0; g = 255; b = 0;
                          snd = 4;
                          break;
                        case '2':  // blue
                        case 'B':
                          r = 0; g = 0; b = 255;
                          snd = 5;
                          break;
                        default:  // yellow
                          r = 255; g = 255; b = 0;
                          snd = 6;
                          break;
                      }
                      switch (argsStr[1]) {
                        case 'L':
                          globalEvent.setBeginColor( EAR_LEFT, r, g, b );
                          globalEvent.setBeginColor( EAR_RIGHT, 0, 0, 0 );
                          break;
                        case 'R':
                          globalEvent.setBeginColor( EAR_LEFT, 0, 0, 0 );
                          globalEvent.setBeginColor( EAR_RIGHT, r, g, b );
                          break;
                        default:  // both
                          globalEvent.setBeginColor( EAR_LEFT, r, g, b );
                          globalEvent.setBeginColor( EAR_RIGHT, r, g, b );
                          break;
                      }
                      
                      currentPattern=&setColor;
                      currentPattern->localCounter = 0;
                      currentPattern->processDelay = globalEvent.pulse_rate = 20;
                      mp3_play(snd);
                    }
                break;
                case 'm':
                    printGlobalEvent( globalEvent );
                break;
                case 'O':
                    if ( argsStr.toInt() != 0 ) 
                    {
                        globalEvent.checkForPanicMode = true;
                        globalEvent.wasConnected = true;
                    }
                    else
                    {
                        globalEvent.checkForPanicMode = false;                    
                    }
                break;
                case 'P':
                    currentPattern=&pulseColor;
                break;
                case 'p':
                    currentPattern=&pingPong;
                break;
                case 'Q':
                    parseColor( argsStr );
                    currentPattern=&setColor;
                    currentPattern->localCounter = 0;
                    currentPattern->processDelay = globalEvent.pulse_rate;
                break;
                case 'R':
                    globalEvent.pulse_rate = argsStr.toInt();
                    globalEvent.pulse_dir = globalEvent.pulse_rate;
                break;
                case 'S':
                {
                    i_value = argsStr.toInt();
                    if (i_value == 0)
                        mp3_stop();
                    else
                        mp3_play(i_value);
    //                Serial.print("Playing Track");
    //                Serial.println(i_value);
    //                PlaySound playSound;
    //                playSound.playTrack( i_value );
                }
                break;
                case 'T': // Change the timer even pulse_rate
                {
                    t.stop(timerId);
                    i_value = argsStr.toInt();
                    //Timer1.setPeriod(i_value);
    
                    timerId = t.every(i_value, processEvent);
                    Serial.print("Timer=");
                    Serial.println(i_value);
                }
                break;
                case 'V':
                {
                    i_value = argsStr.toInt();
                    mp3_set_volume(i_value);
                }
                break;
                case 'z':
                     currentPattern=&bootSequence;
                     currentPattern->initialize( globalEvent );
                break;
                case 'Z':
                     setLed( EAR_LEFT, 0, 0, 0 );
                     setLed( EAR_RIGHT, 0, 0, 0 );
                     clearAllColors( globalEvent );
                     currentPattern = NULL;
                break;
    
                case 'H':
                     printHelp();
                break;
            }
    
            // Write data back to the console
    //        Serial.write((uint8_t*)buffer, bufferCounter); 
    //        Serial.println("");
    //        Serial.print("Str=");
    //        Serial.println(argsStr);
    //        Serial.print("Command=");
    //        Serial.println(command); 
    
            // Reset the buffer counter
            bufferCounter=0;
        }
    }
}

void parseColor( const String &argsStr )
{
     int i = 0;
//     Serial.println("Process Color");
//     Serial.println(argsStr);
     
     // Determine which ear is to be modified
     char colorEarModifier = argsStr.charAt(i);
     int earMode=0;
     switch ( colorEarModifier )
     {
         case 'L':
             earMode=EAR_LEFT;
             ++i;
         break;
         case 'R':
             earMode=EAR_RIGHT;
             ++i;
         break;
         case 'B':
             earMode=EAR_BOTH;
             ++i;
         break;
         default:
             earMode=EAR_BOTH;
         break;
     }
     
     // Determine which bank of color is to be modified
     // S=Start, E=End, B=Both
     char colorBankModifier = argsStr.charAt(i);
     int colorBankMode=0;
     switch ( colorBankModifier )
     {
         default:
             colorBankMode=COLOR_BANK_START;
         break;
         case 'S':
             colorBankMode=COLOR_BANK_START;
             ++i;
         break;
         case 'E':
             colorBankMode=COLOR_BANK_END;
             ++i;
         break;
         case 'B':
             colorBankMode=COLOR_BANK_BOTH;
             ++i;
         break;
     }
     
     int colorRedIdx      = argsStr.indexOf( ',', i);
     int colorGreenIdx    = argsStr.indexOf( ',', colorRedIdx + 1 );
     int colorBlueIdx     = argsStr.indexOf( ',', colorGreenIdx + 1 );

//     Serial.print("i=");
//     Serial.print(i);
//     Serial.print("Ri=");
//     Serial.print(colorRedIdx);
//     Serial.print(" Gi=");
//     Serial.print(colorGreenIdx);
//     Serial.print(" Bi=");
//     Serial.println(colorBlueIdx);
     
     const String argsStrRed = argsStr.substring(i, colorRedIdx);
     const String argsStrGreen = argsStr.substring(colorRedIdx+1, colorGreenIdx);
     String argsStrBlue;
     if (colorBlueIdx==-1) {
         // didn't get a comma after blue
         argsStrBlue = argsStr.substring(colorGreenIdx+1);         
     } else {
        // got a comma after blue
         argsStrBlue = argsStr.substring(colorGreenIdx+1, colorBlueIdx );
         
         //Set the Duration
         const String argsStrDuration = argsStr.substring(colorBlueIdx+1);
         globalEvent.pulse_rate = argsStrDuration.toInt();
     }

//     Serial.print("Rc=");
//     Serial.print(argsStrRed);
//     Serial.print(" Gc=");
//     Serial.print(argsStrGreen);
//     Serial.print(" Bc=");
//     Serial.println(argsStrBlue);
     
     int color_red = argsStrRed.toInt();
     int color_green = argsStrGreen.toInt();
     int color_blue = argsStrBlue.toInt();
     
     if ( earMode == EAR_LEFT || earMode == EAR_BOTH ) {
         switch ( colorBankMode ) {
             default:
             case COLOR_BANK_START:
                 globalEvent.setBeginColor( EAR_LEFT, color_red, color_green, color_blue );
             break;
             case COLOR_BANK_END:
                 globalEvent.setEndColor( EAR_LEFT, color_red, color_green, color_blue );
             break;
             case COLOR_BANK_BOTH:
                 globalEvent.setColor( EAR_LEFT, color_red, color_green, color_blue );
             break;
         }
     }
     if ( earMode == EAR_RIGHT || earMode == EAR_BOTH )
     {
         switch ( colorBankMode ) 
         {
             default:
             case COLOR_BANK_START:
                 globalEvent.setBeginColor( EAR_RIGHT, color_red, color_green, color_blue );
             break;
             case COLOR_BANK_END:
                 globalEvent.setEndColor( EAR_RIGHT, color_red, color_green, color_blue );
             break;
             case COLOR_BANK_BOTH:
                 globalEvent.setColor( EAR_RIGHT, color_red, color_green, color_blue );
             break;
         }
     }
     
//     Serial.print(color_red);
//     Serial.print(",");
//     Serial.print(color_green);
//     Serial.print(",");
//     Serial.print(color_blue);
//     Serial.print(" Bank=");
//     Serial.println(colorBankMode);
}


