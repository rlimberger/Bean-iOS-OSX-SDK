//
//  ViewController.swift
//  hackDayTemplate
//
//  Created by Rene Limberger on 9/23/14.
//  Copyright (c) 2014 Walt Disney Animation. All rights reserved.
//

import UIKit
import AudioToolbox

class SimonViewController: UIViewController, PTDBeanManagerDelegate, PTDBeanDelegate {
    
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var yellowButton: UIButton!
    
    @IBOutlet weak var leftEar: UIImageView!
    @IBOutlet weak var rightEar: UIImageView!

    @IBOutlet weak var beginButton: UIButton!
    @IBOutlet weak var speak: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var gameStartLabel: UILabel!
    @IBOutlet weak var gameOverLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var highScoreLabel: UILabel!

    //let WDASbeanName = "WDAS Bean 3" // 2-Finished hat
    let WDASbeanName = "WDAS Bean"     // All nearby hats
    let MYbeanName = "WDAS Bean 2"     // All nearby hats

    let beanManager = PTDBeanManager()
    var myBean: PTDBean?
    var connectedBeans: [PTDBean] = []
    var localSounds: [SystemSoundID] = []
    var localGameEndSound: SystemSoundID = 0
    
    let colors = [UIColor.redColor(), UIColor.greenColor(), UIColor.blueColor(), UIColor.yellowColor()]
    let imagesLeft = [UIImage(named:"LeftRed"),UIImage(named:"LeftGreen"),UIImage(named:"LeftBlue"),UIImage(named:"LeftYellow")]
    let imagesRight = [UIImage(named:"RightRed"),UIImage(named:"RightGreen"),UIImage(named:"RightBlue"),UIImage(named:"RightYellow")]
    let sounds = [3,4,5,6]
    let gameStartSound = 1
    let gameEndSound = 7
    
    var currentColorIndex = 0
    var currentSoundIndex = 0
    
    var highScore = 0
    var curScore = 0
    
    var sequence: [Int] = []
    var currentSequenceIndex = 0;
    var readyToAdvance = false
    var stillPlaying = false
    var endingGame = false
    
    var timerDuration = 1.0
    var colorDuration = 15
    
    var usersequence: [Int] = []
    
    func loadSound(soundName: String) -> SystemSoundID {
        let soundURL = NSBundle.mainBundle().URLForResource(soundName, withExtension: "caf")
        var mySound: SystemSoundID = 0
        AudioServicesCreateSystemSoundID(soundURL, &mySound)
        println("Loaded Sound \(soundName), URL: \(soundURL) with ID: \(mySound)")
        return mySound
    }
    
    func loadAllSounds() {
        localSounds.append( loadSound("simon_r") )
        localSounds.append( loadSound("simon_g") )
        localSounds.append( loadSound("simon_b") )
        localSounds.append( loadSound("simon_y") )
        localGameEndSound = loadSound("simon_lose")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAllSounds()
        beanManager.delegate = self
        leftEar.hidden = true
        rightEar.hidden = true
        gameOverLabel.hidden = true
    }
    
    func regenerateSequence()
    {
        var rand = Int(arc4random_uniform(UInt32(4)))
        sequence.append( rand )
        currentSequenceIndex = 0
        println("Generated Sequence: \(sequence)" )
    }
    
    func gameOver() -> Bool {
        println("WRONG!  Game Over");
        stillPlaying = false
        endingGame = true
        gameOverLabel.hidden = false
        gameStartLabel.hidden = true
        leftEar.hidden = true
        rightEar.hidden = true
        
        if curScore > highScore { highScore = curScore }
        highScoreLabel.text = "\(highScore)"
        
        if let bean = myBean {
            var strC = "QBS255,0,0,0\n"
            bean.sendSerialString(strC)
            var strS = "S\(gameEndSound)\n"
            bean.sendSerialString(strS)
        }
        
        AudioServicesPlaySystemSound(localGameEndSound)
        NSTimer.scheduledTimerWithTimeInterval(3.0, target:self, selector:Selector("gameOverTimerFinished"), userInfo:nil, repeats:false)
        return false
    }
    
    func validateGame() -> Bool {
        if usersequence.count == 0 { return false; }
        //println( sequence )
        //println( usersequence )
        for index in 0...usersequence.count-1 {
            //println ("Comparing: \(sequence[index]) to \(usersequence[index])")
            if sequence[index] != usersequence[index] { return false; }
        }
        return true
    }
    
    func clearColors() {
        leftEar.hidden = true
        rightEar.hidden = true
        for bean in connectedBeans {
            bean.sendSerialString("Z\n")
        }
    }
    
    func setSound(soundIndex: Int) {
        // send sound code
        for bean in connectedBeans {
            var str = "S\(soundIndex)\n"
            bean.sendSerialString(str)
            println("Sending Sound:: >\(str)<")
        }
    }
    
    func setColor(colorIndex: Int, duration: Int = 0, which: Int = 0) {
        // color the output debug button
        if which == 0 { // LEFT
            leftEar.image = imagesLeft[colorIndex]
            leftEar.hidden = false
            rightEar.hidden = true
        } else if which == 1 {  // RIGHT
            rightEar.image = imagesRight[colorIndex]
            leftEar.hidden = true
            rightEar.hidden = false
        }
        else {
            leftEar.hidden = true;
            rightEar.hidden = true;
        }
        
        for bean in connectedBeans {
            
            // Grab the UIColor
            let color = colors[colorIndex]
            //bean.setLedColor(color)
            
            // extract rgb values from UIColor
            var r: CGFloat = 0
            var g: CGFloat = 0
            var b: CGFloat = 0
            var a: CGFloat = 0
            color.getRed(&r, green: &g, blue: &b, alpha: &a)
            
            // send color command to bean
            if which == 0 { // LEFT
                //var str = "QLB\(Int(255.0*r)),\(Int(255.0*g)),\(Int(255.0*b))\n"
                var str = "QLS\(Int(255.0*r)),\(Int(255.0*g)),\(Int(255.0*b)),\(Int(duration)))\n"
                println("Sending LEFT:: >\(str)<")
                bean.sendSerialString(str)
            }
            else if which == 1 { // RIGHT
                //var str = "QRB\(Int(255.0*r)),\(Int(255.0*g)),\(Int(255.0*b))\n"
                var str = "QRS\(Int(255.0*r)),\(Int(255.0*g)),\(Int(255.0*b)),\(Int(duration)))\n"
                println("Sending RIGHT:: >\(str)<")
                bean.sendSerialString(str)
            }
            else {  // BOTH
                //var str = "QRB\(Int(255.0*r)),\(Int(255.0*g)),\(Int(255.0*b))\n"
                var str = "QBS\(Int(255.0*r)),\(Int(255.0*g)),\(Int(255.0*b)),0\n"
                println("Sending BOTH:: >\(str)<")
                bean.sendSerialString(str)
            }
            

        }
        // Play the sound for the color
        setSound(sounds[colorIndex])
    }
    
    func gameOverTimerFinished() {
        println("Resetting Game")
        gameOverLabel.hidden = true
        gameStartLabel.hidden = false
        endingGame = false
        clearColors()
    }
    
    func timerSequenceAdvance() {
        if ( !stillPlaying ) { return; } // Game Over before timer expired
        
        // NOTE: this happens when we need to clear the color
        if (currentSequenceIndex == sequence.count) {
            clearColors()
            advanceToNextRound()
            return
        }
        
        var which = (currentSequenceIndex % 2)
        currentColorIndex = sequence[currentSequenceIndex]
        if which == 0 {
            println("Playing Left Index: \(currentSequenceIndex) with color: \(currentColorIndex)")
        } else if which == 1 {
            println("Playing Right Index: \(currentSequenceIndex) with color: \(currentColorIndex)")
        }
        setColor(currentColorIndex, duration:colorDuration, which:which)
        if connectedBeans.count == 0 { // No beans connected, play sound locally
            AudioServicesPlaySystemSound(localSounds[currentColorIndex])
        }
        
        currentSequenceIndex++;
        if currentSequenceIndex <= sequence.count {
            if !stillPlaying { currentSequenceIndex = sequence.count } // Bail on further playback
            NSTimer.scheduledTimerWithTimeInterval(timerDuration, target:self, selector:Selector("timerSequenceAdvance"), userInfo:nil, repeats:false)
        }
     }
    
    func advanceToNextRound() {
        if !readyToAdvance {
            println("Completed first requirement to advance to next round")
            readyToAdvance = true
        } else {
            if !validateGame() { gameOver() }
            else {
                println("Advancing to next round")
                curScore++
                scoreLabel.text = "\(curScore)"
                regenerateSequence();
                readyToAdvance = false
                usersequence = []
                if timerDuration > 0.05 { timerDuration -= 0.05 }
                NSTimer.scheduledTimerWithTimeInterval(0.5, target:self, selector:Selector("timerSequenceAdvance"), userInfo:nil, repeats:false)
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func buttonPressed(sender: UIButton) {
        var selectedColorIndex = 0
        switch sender {
        case redButton:
            selectedColorIndex = 0
        case greenButton:
            selectedColorIndex = 1
        case blueButton:
            selectedColorIndex = 2
        case yellowButton:
            selectedColorIndex = 3
        default:
            println("ERROR");
            return
        }
        
        if endingGame { return; }
        
        // Record the button press, and advance to the next round (if needed)
        if stillPlaying && usersequence.count < sequence.count {
            println("Recorded Button: \(selectedColorIndex) ")

            usersequence.append(selectedColorIndex)
            if !validateGame() {
                gameOver()
            }
            else
            {
                //setSound(sounds[selectedColorIndex])
                AudioServicesPlaySystemSound(localSounds[selectedColorIndex])

                // Did we get all of our user sequences yet?
                if usersequence.count == sequence.count { advanceToNextRound() }
            }
        }
        else if ( !stillPlaying ) {
            println("Playing User Button: \(selectedColorIndex)")
            // Allow user to press buttons and send commands to the Hat
            setColor(selectedColorIndex, duration:0, which:2)
            AudioServicesPlaySystemSound(localSounds[selectedColorIndex])
        }
    }
    
    func setupDemoMode() {
        sequence = [0,1,1,2,3]  // DEMO MODE
        println("Generated DEMO Sequence: \(sequence)" )
    }

    @IBAction func startGame(sender: UIButton) {
        if myBean == nil && connectedBeans.count > 0 { myBean = connectedBeans[0] }
        if stillPlaying || endingGame { return; } // Don't allow user to start multiple games at once
        
        println("Starting New Game")

        sequence = []
        usersequence = []
        currentSequenceIndex = 0
        
        regenerateSequence()   // NORMAL
        //setupDemoMode()          // DEMO MODE
        
        clearColors()
        setSound(gameStartSound)

        gameStartLabel.hidden = true
        gameOverLabel.hidden = true
        curScore = 0
        readyToAdvance = false
        stillPlaying = true
        scoreLabel.text = "\(curScore)"
        timerDuration = 1.0
        NSTimer.scheduledTimerWithTimeInterval(3, target:self, selector:Selector("timerSequenceAdvance"), userInfo:nil, repeats:false)
    }

    // MARK: Bean delegate protocol
    // -----------------------------
    
    // callback from bean module when serial data is received.
    func bean(bean: PTDBean, serialDataReceived: NSData) {
        println ("Got serial data from: \(bean.name) >\(NSString(data: serialDataReceived, encoding: NSUTF8StringEncoding))<")
    }
    
    // MARK: BeanManager delegate protocol
    // -----------------------------
    
    func beanManagerDidUpdateState(beanManager: PTDBeanManager) {
        if beanManager.state == BeanManagerState.PoweredOn {
            // start looking for beans
            beanManager.startScanningForBeans_error(nil)
            
            // start spinning nose and hide the ears
            activityIndicator.startAnimating()
        }
    }
    
    func BeanManager(beanManager: PTDBeanManager, didDiscoverBean bean: PTDBean, error: NSError) {
        // check for known name
        if bean.name.hasPrefix(WDASbeanName) {
            // connect now
            beanManager.connectToBean(bean, error: nil)
        }
    }
    
    func BeanManager(beanManager: PTDBeanManager, didConnectToBean bean: PTDBean, error: NSError) {
        println("SUCCESS!  Connected to Bean: \(bean.name)")

        if myBean == nil && bean.name == MYbeanName { myBean = bean }
        bean.delegate = self
        //bean.sendSerialString("Z\n")
        connectedBeans.append(bean)

        // stop spinning nose as soon as we get at least one bean connected
        activityIndicator.stopAnimating()
        gameStartLabel.hidden = false
    }
    
    func BeanManager(beanManager: PTDBeanManager, didDisconnectBean bean: PTDBean, error: NSError) {
        println("SUCCESS!  Disconnected from Bean: \(bean.name)")

        if myBean == bean { myBean = nil }
        bean.delegate = nil
        for index in 0...connectedBeans.count-1 {
            if connectedBeans[index] == bean { connectedBeans.removeAtIndex(index); break; }
        }
        
        // Start spinning nose again as soon as we get lose all beans
        if connectedBeans.count == 0 {
            activityIndicator.startAnimating()
        }
    }

}

