// Libraries
#include "Timer.h"

// Project libraries
#include "initialize.h"
#include "patternLib.h"
#include "eventLib.h"
#include "commandProcessing.h"

PingPong pingPong;
CycleColors cycleColors;
BlinkColors blinkColors;
TurnEarsOff turnEarsOff;
PulseColor pulseColor;
HatAlarm hatAlarm;
BootSequence bootSequence;
SetColor setColor;

Pattern *currentPattern = NULL;

ProcessEvent globalEvent;

void setup()
{
    initializeSerial();
    initializePWM();
    initializeTimer();
    initializeDFRobot();
    bootHat();
}

void loop()
{
     processInput();
     t.update();
}

