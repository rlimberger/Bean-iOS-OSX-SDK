

#include <SoftPWM.h>

void initializePWM()
{
    SoftPWMBegin(SOFTPWM_INVERTED);
    
    SoftPWMSet(LEFT_RED_PIN, 0);
    SoftPWMSet(LEFT_GREEN_PIN, 0);
    SoftPWMSet(LEFT_BLUE_PIN, 0);
    SoftPWMSet(RIGHT_RED_PIN, 0);
    SoftPWMSet(RIGHT_GREEN_PIN, 0);
    SoftPWMSet(RIGHT_BLUE_PIN, 0);
}


void initializeSerial()
{
    // initialize serial communication at 57600 bits per second:
    Serial.begin(57600);
    // this makes it so that the arduino read function returns
    // immediatly if there are no less bytes than asked for.
    //Serial.setTimeout(25); 
    Serial.setTimeout(25);
}

// Go through the boot process when the bean is initialized
void bootHat()
{
    // Pause before starting patterns
    delay(100);
    currentPattern=&bootSequence;
    currentPattern->initialize( globalEvent );;
}



//Initialize a timer that processes the event loop at a pre determined interval
void initializeTimer()
{
    timerId = t.every(EVENT_TIMER, processEvent); 
}
