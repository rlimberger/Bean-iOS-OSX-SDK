

#ifndef INITIALIZE_LIB_H
#define INITIALIZE_LIB_H

void initializePWM();
void bootHat();
void processEvent();
void initializeSerial();
void initializeTimer();

#endif
