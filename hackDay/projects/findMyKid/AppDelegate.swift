//
//  AppDelegate.swift
//  hackDayTemplate
//
//  Created by Rene Limberger on 9/23/14.
//  Copyright (c) 2014 Walt Disney Animation. All rights reserved.
//

import UIKit

let kMyBean = "WDAS Bean 3"

let kBeanStartedSearch = "BeanStartedSearch"
let kBeanDiscovered = "BeanDiscovered"
let kBeanConnected = "BeanConnected"
let kBeanDisconnected = "BeanDisconnected"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PTDBeanManagerDelegate {
    
    let beanManager = PTDBeanManager()
    let currentBeans = NSMutableSet()
    var FAKEDisconnect: Bool = false
    
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        NSLog("----Starting up, beanManager: \(beanManager)")
        beanManager.delegate = self

        //Go to register or ask permission from the user for firing local notifications
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Sound | UIUserNotificationType.Alert |
            UIUserNotificationType.Badge, categories: nil))
        
        return true
    }

    func beanManagerDidUpdateState(beanManager: PTDBeanManager) {
        if beanManager.state == BeanManagerState.PoweredOn {
            // start looking for beans
            beanManager.startScanningForBeans_error(nil)
            NSLog("----Started Search")
            NSNotificationCenter.defaultCenter().postNotificationName(kBeanStartedSearch, object:nil)
        }
    }
    
    func BeanManager(beanManager: PTDBeanManager, didDiscoverBean bean: PTDBean, error: NSError) {
        // check for known name
        NSLog("----Discovered bean '\(bean.name)'")
//        if bean.name == "iPad" || bean.name == "mini" || bean.name.rangeOfString(kMyBean) != nil {
        if bean.name.rangeOfString(kMyBean) != nil {
            // connect now
            NSLog("----try to connect to bean '\(bean.name)'")
            NSNotificationCenter.defaultCenter().postNotificationName(kBeanDiscovered, object:nil)
            beanManager.connectToBean(bean, error: nil)
        }
    }
    
    func BeanManager(beanManager: PTDBeanManager, didConnectToBean bean: PTDBean, error: NSError) {
        currentBeans.addObject(bean)
        NSLog("----Connected to bean '\(bean.name)'")
//        var cmd = "O1\n"
//        NSLog("---sending command '\(cmd)' to bean '\(bean.name)'")
//        // send enable panic to bean
//        bean.sendSerialString(cmd)
        NSNotificationCenter.defaultCenter().postNotificationName(kBeanConnected, object:nil)
    }
    
    func BeanManager(beanManager: PTDBeanManager, didDisconnectBean bean: PTDBean, error: NSError) {
        currentBeans.removeObject(bean)
        NSLog("----Disconnected from bean '\(bean.name)'")
        if bean.name.rangeOfString(kMyBean) != nil {
//        if bean.name == "iPad" || bean.name == "mini" || bean.name.rangeOfString(kMyBean) != nil {
            NSNotificationCenter.defaultCenter().postNotificationName(kBeanDisconnected, object:nil)
            var localNotification:UILocalNotification = UILocalNotification()
            localNotification.alertAction = "open app"
            localNotification.alertBody = "Your family member is out of range!!!"
            //localNotification.soundName = UILocalNotificationDefaultSoundName
            localNotification.soundName = "panic.caf"
            localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)

            if (!FAKEDisconnect) {
                // Look again
                beanManager.startScanningForBeans_error(nil)
                NSLog("----Started Search")
                NSNotificationCenter.defaultCenter().postNotificationName(kBeanStartedSearch, object:nil)
            }
        }
    }

    // TEST METHODS - REMOVE WHEN HAVE HARDWARE FOR TESTING
    func testStartSearch() {
        NSNotificationCenter.defaultCenter().postNotificationName(kBeanStartedSearch, object:nil)
    }
    
    func testConnect() {
        NSNotificationCenter.defaultCenter().postNotificationName(kBeanConnected, object:nil)
    }
    
    func testLostConnection() {
        NSNotificationCenter.defaultCenter().postNotificationName(kBeanDisconnected, object:nil)
    }

    func testDisconnect() {
        if FAKEDisconnect {
            NSLog("==============================WE are FAKE disconnected, reconnected")
            FAKEDisconnect = false
            NSNotificationCenter.defaultCenter().postNotificationName(kBeanConnected, object:nil)
            var cmd = "I0\n"
            for bean in currentBeans {
                var theBean: PTDBean = bean as PTDBean
                var error: NSError
                NSLog("---sending command '\(cmd)' to bean '\(bean.name)'")
                // send enable panic to bean
                bean.sendSerialString(cmd)
            }
        }
        else {
            NSLog("==============================WE are FAKE connected, disconnect")
            FAKEDisconnect = true
            NSNotificationCenter.defaultCenter().postNotificationName(kBeanDisconnected, object:nil)
            var cmd = "I1\n"
            for bean in currentBeans {
                var theBean: PTDBean = bean as PTDBean
                var error: NSError
                NSLog("---sending command '\(cmd)' to bean '\(bean.name)'")
                // send enable panic to bean
                bean.sendSerialString(cmd)
            }
        }
//        FAKEDisconnect = true
//        for bean in currentBeans {
//            var theBean: PTDBean = bean as PTDBean
//            var error: NSError
//            beanManager.disconnectBean(theBean, error:nil)
//        }
//        dispatch_after_delay(5.0, queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {
//                self.FAKEDisconnect = true
//                self.beanManager.startScanningForBeans_error(nil)
//            }
    }
    
    func dispatch_after_delay(delay: NSTimeInterval, queue: dispatch_queue_t, block: dispatch_block_t) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(time, queue, block)
    }
}

