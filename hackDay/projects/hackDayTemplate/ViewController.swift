//
//  ViewController.swift
//  hackDayTemplate
//
//  Created by Rene Limberger on 9/23/14.
//  Copyright (c) 2014 Walt Disney Animation. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PTDBeanManagerDelegate,PTDBeanDelegate, UITextFieldDelegate {
    
    let beanManager = PTDBeanManager()
    var currentBean: PTDBean?
    
    let WDASbeanName = "WDAS Bean" // Hard-code to the hat to control
    
    @IBOutlet weak var testRightEar: UIButton!
    @IBOutlet weak var testLeftEar: UIButton!
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var refColorButton: UIButton!
    @IBOutlet weak var sendColorButton: UIButton!
    
    @IBOutlet weak var pattern1Button: UIButton!
    @IBOutlet weak var pattern2Button: UIButton!
    @IBOutlet weak var pattern3Button: UIButton!
    
    @IBOutlet weak var textBox: UITextField!
    @IBOutlet weak var sendText: UIButton!
    
    @IBOutlet weak var connectedBeanLabel: UILabel!
    
    let letItGoString: String = "1\n"
    let pattern1String: String = "S1\n"
    let pattern2String: String = "QBB255,135,255\n"
    let pattern3String: String = "z\n"
    
    @IBOutlet weak var alarmButton: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let colors = [UIColor.redColor(),  UIColor.greenColor(), UIColor.blueColor()]
    var currentColorIndex = 0
    
    var currentRed:CGFloat = 1.0
    var currentGreen:CGFloat = 0.0
    var currentBlue:CGFloat = 0.0
    
    var currentSoundIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        beanManager.delegate = self
        
        testLeftEar.backgroundColor = UIColor.blackColor()
        testRightEar.backgroundColor = UIColor.blackColor()
        refColorButton.backgroundColor = UIColor.redColor()
        
        redSlider.minimumValue = 0
        redSlider.maximumValue = 1.0
        redSlider.value = 1
        
        greenSlider.minimumValue = 0
        greenSlider.maximumValue = 1.0
        greenSlider.value = 0
        
        blueSlider.minimumValue = 0
        blueSlider.maximumValue = 1.0
        blueSlider.value = 0
        
        textBox.delegate = self
        textBox.clearButtonMode = UITextFieldViewMode.Always
        textBox.clearsOnBeginEditing = true
        
        connectedBeanLabel.text = ""
    }
    
    // MARK: IBActions
    @IBAction func sliderChanged(sender: UISlider){
        var sliderVal = CGFloat(sender.value);
        switch sender{
        case redSlider:
            currentRed = sliderVal
        case greenSlider:
            currentGreen = sliderVal
        case blueSlider:
            currentBlue = sliderVal
        default:
            println("ERROR");
            return
        }
        var newColor = UIColor(red:currentRed, green: currentGreen, blue: currentBlue, alpha: 1);
        refColorButton.backgroundColor = newColor;
    }
    
    @IBAction func setColorButton(sender: UIButton){
        var a: CGFloat = 1
        var newColor = UIColor(red:currentRed, green: currentGreen, blue: currentBlue, alpha: 1);
        setColor(newColor)
    }
    
    func setColor(color: UIColor, duration: Int = 0, ear: String = "B") {
        // extract rgb values from UIColor
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rf: Float = 255*Float(r)
        let gf: Float = 255*Float(g)
        let bf: Float = 255*Float(b)
        
        let rI: Int = Int(rf)
        let gI: Int = Int(gf)
        let bI: Int = Int(bf)
        
        let colorString:String = "Q"+ear+"B"+"\(rI),\(gI),\(bI),\(duration)\n"
        println(colorString);
        setDebugColor(color, ear: ear)
        
        // send color command to bean
        if let bean = currentBean {
            bean.sendSerialString(colorString)
        }
    }
    
    @IBAction func setPattern(sender: UIButton)
    {
        var pString: String = ""
        switch sender{
        case pattern1Button:
            pString = pattern1String
        case pattern2Button:
            pString = pattern2String
        case pattern3Button:
            pString = pattern3String
        default:
            println("Unrecognized button")
        }
        
        println(pString)
        if let bean = currentBean{
            bean.sendSerialString(pString)
        }
    }
    
    @IBAction func letItGo(sender: UIButton){
        
        println(letItGoString)
        if let bean = currentBean{
            bean.sendSerialString(letItGoString)
        }
        setDebugColor(colors[2], ear:"B");
    }
    
    func setDebugColor(color: UIColor, ear: String){
        if(ear == "B"){
            testRightEar.backgroundColor = color
            testLeftEar.backgroundColor = color
        }else if(ear == "L"){
            testLeftEar.backgroundColor = color
        }else if(ear == "R"){
            testRightEar.backgroundColor = color
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    @IBAction func sendText(sender: UIButton)    {
        var txt: String = textBox.text
        txt = txt+"\n"
        
        println(txt)
        if let bean = currentBean{
            bean.sendSerialString(txt)
        }

    }
    
    // MARK: MeanManager delegate protocol
    // MARK: Bean delegate protocol
    // -----------------------------
    
    // callback from bean module when serial data is received.
    func bean(bean: PTDBean, serialDataReceived: NSData) {
        println ("Got serial data: >\(NSString(data: serialDataReceived, encoding: NSUTF8StringEncoding))<")
    }
    
    // MARK: BeanManager delegate protocol
    // -----------------------------
    
    func beanManagerDidUpdateState(beanManager: PTDBeanManager) {
        if beanManager.state == BeanManagerState.PoweredOn {
            // start looking for beans
            beanManager.startScanningForBeans_error(nil)
            
            // start spinning nose
            activityIndicator.startAnimating()
            connectedBeanLabel.text = "looking for bean"
        }
        
        if beanManager.state == BeanManagerState.PoweredOff {
            connectedBeanLabel.text = "bluetooth off"
        }
    }
    
    func BeanManager(beanManager: PTDBeanManager, didDiscoverBean bean: PTDBean, error: NSError) {
        // check for known name
        if bean.name.hasPrefix( WDASbeanName) {
            // connect now
            beanManager.connectToBean(bean, error: nil)
        }
    }
    
    func BeanManager(beanManager: PTDBeanManager, didConnectToBean bean: PTDBean, error: NSError) {
        println("SUCCESS!  Connected to Bean")
        //showButtons()
        currentBean = bean
        bean.delegate = self
        bean.sendSerialString("Z\n")
        
        connectedBeanLabel.text = bean.name
        
        // stop spinning nose
        activityIndicator.stopAnimating()
    }
    
    func BeanManager(beanManager: PTDBeanManager, didDisconnectBean bean: PTDBean, error: NSError) {
        println("SUCCESS!  Disconnected from Bean")
        //hideButtons()
        currentBean = nil
        bean.delegate = nil
        activityIndicator.startAnimating()
        connectedBeanLabel.text = "disconnected"
    }
}
