

void clearAllColors( ProcessEvent& globalEvent )
{
    globalEvent.setBeginColor( EAR_LEFT, 0, 0, 0 );
    globalEvent.setEndColor( EAR_LEFT, 0, 0, 0 );
    globalEvent.setBeginColor( EAR_RIGHT, 0, 0, 0 );
    globalEvent.setEndColor( EAR_RIGHT, 0, 0, 0 );
}

void setLed( int ear, uint8_t red, uint8_t green, uint8_t blue )
{
    if (debug)
    {
        Serial.print("Color=");
        Serial.print(red);
        Serial.print(" ");
        Serial.print(green);
        Serial.print(" ");    
        Serial.println(blue);  
    }
    
    switch ( ear )
    {
        case EAR_LEFT:
        {
            SoftPWMSet(LEFT_RED_PIN, red);
            SoftPWMSet(LEFT_GREEN_PIN, green);
            SoftPWMSet(LEFT_BLUE_PIN, blue);
        } 
        break;
        case EAR_RIGHT:
        {
            SoftPWMSet(RIGHT_RED_PIN, red);
            SoftPWMSet(RIGHT_GREEN_PIN, green);
            SoftPWMSet(RIGHT_BLUE_PIN, blue);
        }
        break;
    }
}

//void Alarm::processTick()
//{
//    vibrate.process();
//    pingPong.processAnimation( &globalEvent );
//    Serial.println("Vibrate Hat");
//}

void BootSequence::processAnimation( ProcessEvent *globalData )
{    
     int bootRate = globalData->pulse_rate*4;
     if ( localCounter >= 0 && localCounter < bootRate ) {
        pingPong.processAnimation( globalData );
     } else if ( localCounter >= bootRate && localCounter < bootRate*2 ) {
        cycleColors.processAnimation( globalData );
     } else if ( localCounter >= bootRate*2 && localCounter < bootRate*3 ) {
        blinkColors.processAnimation( globalData );
     }
#if 0 
     else if ( localCounter >= bootRate*3 ) {
        turnEarsOff.processAnimation( globalData );
     }
#endif
     localCounter++;
     if ( localCounter >= bootRate*3  /*4*/ )
         localCounter = 0;
}

void PulseColor::processAnimation( ProcessEvent *globalData )
{     
 
     //Change the direction when the count is > 255 and < 0
    if (localCounter>=globalData->pulse_rate)
         globalData->pulse_dir = -1;
    if (localCounter<=0)
         globalData->pulse_dir = 1;
    
    // Increment the counter
    localCounter += globalData->pulse_dir; 
         
    // Compute the new colors
    float scale = (float)localCounter/globalData->pulse_rate;
    
    uint8_t red;
    uint8_t green;
    uint8_t blue;

    red   = (uint8_t) ((float)globalData->colorStartLeft[0]*scale+(float)globalData->colorEndLeft[0]*(1-scale));
    green = (uint8_t) ((float)globalData->colorStartLeft[1]*scale+(float)globalData->colorEndLeft[1]*(1-scale));
    blue  = (uint8_t) ((float)globalData->colorStartLeft[2]*scale+(float)globalData->colorEndLeft[2]*(1-scale));
    setLed( EAR_LEFT, red, green, blue );

    red   = (uint8_t) ((float)globalData->colorStartRight[0]*scale+(float)globalData->colorEndRight[0]*(1-scale));
    green = (uint8_t) ((float)globalData->colorStartRight[1]*scale+(float)globalData->colorEndRight[1]*(1-scale));
    blue  = (uint8_t) ((float)globalData->colorStartRight[2]*scale+(float)globalData->colorEndRight[2]*(1-scale));
    setLed( EAR_RIGHT, red, green, blue );
             
    if ( debug ){
        Serial.print("Pulse=");
        Serial.print(int(globalData->pulse_rate));
        Serial.print(" ");
        Serial.print(int(globalData->pulse_dir));
        Serial.print(" ");    
        Serial.println(globalData->pulseCount);  
      
        Serial.println("");
        
        Serial.print("Color=");
        Serial.print(red);
        Serial.print(" ");
        Serial.print(green);
        Serial.print(" ");    
        Serial.println(blue);  
    }
}
