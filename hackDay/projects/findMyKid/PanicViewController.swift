//
//  PanicViewController.swift
//  findMyKid
//
//  Created by Roy Turner on 9/27/14.
//  Copyright (c) 2014 Walt Disney Animation. All rights reserved.
//

import UIKit
import AVFoundation

class PanicViewController: UIViewController {

    var audioPlayer = AVAudioPlayer()
    var coinSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("panic", ofType: "caf")!)

    override func viewDidLoad() {
        audioPlayer = AVAudioPlayer(contentsOfURL: coinSound, error: nil)
        audioPlayer.prepareToPlay()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        audioPlayer.play()
        addObservers()
    }
    
    override func viewDidDisappear(animated: Bool) {
        removeObservers()
        audioPlayer.stop()
    }
    
    func addObservers() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"didConnectToBean:", name:kBeanConnected, object:nil)
    }
    
    func removeObservers() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func didConnectToBean(object: NSNotification) {
        dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func testConnect(sender: UIButton) {
        (UIApplication.sharedApplication().delegate as AppDelegate).testDisconnect()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
