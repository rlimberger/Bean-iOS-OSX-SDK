//
//  ViewController.swift
//  hackDayTemplate
//
//  Created by Rene Limberger on 9/23/14.
//  Copyright (c) 2014 Walt Disney Animation. All rights reserved.
//

import UIKit

class ViewController: UIViewController /*, PTDBeanManagerDelegate*/ {
    
//    let beanManager = PTDBeanManager()
//    let currentBeans = NSMutableSet()
    
    @IBOutlet weak var lookingView: UIView!
    @IBOutlet weak var balloonView: UIView!
    @IBOutlet weak var ears: UIImageView!
    @IBOutlet weak var activityIndicator1: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator2: UIActivityIndicatorView!
    @IBOutlet weak var testStartSearchButton: UIButton!
    @IBOutlet weak var testConnectButton: UIButton!
    @IBOutlet weak var signalStrength: UIProgressView!
    @IBOutlet weak var signalUpdateTimer: NSTimer!
    @IBOutlet weak var safeLabel: UITextField!
    @IBOutlet weak var dangerLabel: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lookingView.hidden = false
        balloonView.hidden = true
        signalStrength.hidden = true
        safeLabel.hidden = true
        dangerLabel.hidden = true
        NSLog("===View did load")
        self.didStartSearch(nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        addObservers()
    }

    override func viewDidDisappear(animated: Bool) {
        removeObservers()
    }

    func didStartSearch(object: AnyObject!) {
        NSLog("===didStartSearch")
        activityIndicator1.startAnimating()
        activityIndicator2.startAnimating()
    }
    
    func didDiscoverBean(object: AnyObject!) {
        NSLog("===didDiscoverBean")
//        activityIndicator1.stopAnimating()
//        activityIndicator2.stopAnimating()
    }
    
    func didConnectToBean(object: AnyObject!) {
        NSLog("===didConnectToBean")
        activityIndicator1.stopAnimating()
        activityIndicator2.stopAnimating()
        fadeToBalloonScreen()
        println("start timer")
        self.signalUpdateTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target:self, selector:"updateSignalStrength:", userInfo:nil, repeats:true)
    }

    func didLoseConnection(object: AnyObject!) {
        NSLog("===didLoseConnection")
        performSegueWithIdentifier("Panic", sender:self)
        print("disconnected---------------------------------")
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "Test local notification in swift"
        localNotification.alertBody = "All good.  This would be the panic.  We need a custom sound!!!"
        //localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.soundName = "panic.caf"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        if self.signalUpdateTimer != nil {
            self.signalUpdateTimer.invalidate()
            self.signalUpdateTimer = nil
        }
    }

    func addObservers() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"didStartSearch:", name:kBeanStartedSearch, object:nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"didDiscoverBean:", name:kBeanDiscovered, object:nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"didConnectToBean:", name:kBeanConnected, object:nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"didLoseConnection:", name:kBeanDisconnected, object:nil)
    }

    func removeObservers() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: IBActions
    @IBAction func redBalloon(sender: UIButton) {
        NSLog("Red Button pressed");
        self.setColor(.redColor())
        ears.image = UIImage(named:"RedEars")
    }
    
    @IBAction func greenBalloon(sender: UIButton) {
        NSLog("Green Button pressed");
        self.setColor(.greenColor())
        ears.image = UIImage(named:"GreenEars")
    }
    
    @IBAction func blueBalloon(sender: UIButton) {
        NSLog("Blue Button pressed");
        self.setColor(.blueColor())
        ears.image = UIImage(named:"BlueEars")
    }
    
    @IBAction func yellowBalloon(sender: UIButton) {
        NSLog("Yellow Button pressed");
        self.setColor(.yellowColor())
        ears.image = UIImage(named:"YellowEars")
    }
    
    @IBAction func clearBalloon(sender: UIButton) {
        NSLog("Clear Button pressed");
        self.setColor(.blackColor())
        ears.image = UIImage(named:"BlackEars")
    }
    
    func setColor(color: UIColor, duration: Int = 0) {
        
        if color == UIColor.blackColor() {
            for bean in (UIApplication.sharedApplication().delegate as AppDelegate).currentBeans {
                var cmd = "Z\n"
                NSLog("---sending command '\(cmd)' to bean '\(bean.name)'")
                // send color command to bean
                bean.sendSerialString(cmd)
            }
        }
        else {
            for bean in (UIApplication.sharedApplication().delegate as AppDelegate).currentBeans {
            //for bean in currentBeans {
                bean.setLedColor(color)
                
                // extract rgb values from UIColor
                var r: CGFloat = 0
                var g: CGFloat = 0
                var b: CGFloat = 0
                var a: CGFloat = 0
                color.getRed(&r, green: &g, blue: &b, alpha: &a)
                var cmd = "QBB\(Int(r*255)),\(Int(g*255)),\(Int(b*255)),0\n"
                NSLog("---sending command '\(cmd)' to bean '\(bean.name)'")
                // send color command to bean
                bean.sendSerialString(cmd)
            }
        }
    }
    
    func calcProgressValue(signalStrength: Float) -> Float {
        // 60 is close value, 99 far value
        // Map these to 1.0 and 0.0
        let farValue: Float = 99.0
        let closeValue: Float = 60.0
        var progress = -signalStrength
        progress = progress - closeValue
        progress = progress / (farValue - closeValue)
        progress = 1.0 - progress
        if progress < 0.0 {
            progress = 0.0
        }
        if progress > 1.0 {
            progress = 1.0
        }
        return progress
    }

    func updateSignalStrength(userInfo: AnyObject) {
//        println("--Update signal strength")
        
        for bean in (UIApplication.sharedApplication().delegate as AppDelegate).currentBeans {
            bean.readRSSI()
            var value = calcProgressValue(bean.RSSI!.floatValue)
//            println("  Bean \(bean.name) signal: \(bean.RSSI) = progress:\(value)")
            self.signalStrength.progress = value
        }
    }

    func fadeToBalloonScreen() {
        self.lookingView.alpha = 1.0
        self.balloonView.alpha = 0.0
        self.signalStrength.alpha = 0.0
        self.safeLabel.alpha = 0.0
        self.dangerLabel.alpha = 0.0
        self.lookingView.hidden = false
        self.balloonView.hidden = false
        self.safeLabel.hidden = false
        self.dangerLabel.hidden = false
        UIView.animateWithDuration(1.0,
            animations: {() -> Void in
                self.lookingView.alpha = 0.0
                self.balloonView.alpha = 1.0
                self.signalStrength.alpha = 1.0
                self.safeLabel.alpha = 1.0
                self.dangerLabel.alpha = 1.0
            },
            completion: {(Bool) -> Void in
                self.lookingView.hidden = true
                self.balloonView.hidden = false
                self.signalStrength.hidden = false
                self.safeLabel.hidden = false
                self.dangerLabel.hidden = false

                self.activityIndicator1.stopAnimating()
                self.activityIndicator2.stopAnimating()
                self.testStartSearchButton.hidden = true
                self.testConnectButton.hidden = true
            })
    }
    
    // TEST METHODS - REMOVE WHEN HAVE HARDWARE FOR TESTING
    @IBAction func testStartSearch(sender: UIButton) {
        (UIApplication.sharedApplication().delegate as AppDelegate).testStartSearch()
    }
    
    @IBAction func testConnect(sender: UIButton) {
        (UIApplication.sharedApplication().delegate as AppDelegate).testConnect()
    }
    
    @IBAction func testLostConnection(sender: UIButton) {
        (UIApplication.sharedApplication().delegate as AppDelegate).testLostConnection()
    }

    @IBAction func testDisconnect(sender: UIButton) {
        (UIApplication.sharedApplication().delegate as AppDelegate).testDisconnect()
    }
    
}


